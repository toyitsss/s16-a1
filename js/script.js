let count = 1; //ang value ni count ay 1
while(count < 11){ //count (1) is less than 11 = true
	console.log("count is " + count)
	count++ // count (1) is increment by 1 until 10, it will stop to 11.
			//because 11 < 11 is false
}

let num = 1;
while(num <= 10){
    console.log(num)
    num += 2;
}

// Print all number between -10 to 19
let num1 = -10;
while(num1 <= 19){
	console.log(num1)
	num1++
}

// Print all even number bewween 10 to 40
let num2 = 10;
while(num2 <= 40){
	console.log(num2)
	num2+=2
}

let num3 = 301;
while(num3 <= 333){
	console.log(num3)
	num3+=2;
}